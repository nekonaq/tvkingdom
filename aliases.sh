export APP_DIR_TVKINGDOM="$( /bin/pwd )"

@tvkingdom() {
  ( cd "$APP_DIR_TVKINGDOM"
    docker-compose "$@"
  )
}

@tvkingdom-devel() {
  @tvkingdom -f docker-compose-devel.yml "$@"
}

@tvkingdom.build() {
  @tvkingdom -f docker-compose.yml -f docker-compose-build.yml build "$@"
}

@tvkingdom.push() {
  ( . .env
    set -x
    docker push "$REGISTRY_PREFIX$COMPOSE_PROJECT_NAME:$IMAGE_VERSION"
  )
}

# usage:
#  @tvkingdom.build
#
#  @tvkingdom-devel run --rm main /bin/bash
#
#  @tvkingdom-devel run --rm main /bin/bash
#  @tvkingdom-devel run --rm main bin/scrape-static
#  @tvkingdom-devel run --rm main --area bs1
#  @tvkingdom-devel run --rm main --area bs1 --log /app/data/logs
