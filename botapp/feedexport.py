# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from .items import TvProgramme, TvProgrammeChannel


def get_item_uri_params_split_programme(feedexporter, item, spider, **kwargs):
    if isinstance(item, TvProgramme):
        kwargs['item_name'] = 'programme/{}'.format(item['tv_channel_code'])

    if isinstance(item, TvProgrammeChannel):
        kwargs['item_name'] = 'programme/{}'.format(item['code'])

    return kwargs


split_programme = get_item_uri_params_split_programme
