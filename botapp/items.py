# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
import scrapy
from scrapy_extra.utils.items import make_item_loader_method
from .itemloaders import (
    TvAreaItemLoader,
    TvCategoryItemLoader,
    TvChannelItemLoader,
    TvProgrammeSummaryItemLoader,
    TvProgrammeDetailItemLoader,
)


def xmltv_datetime(value):
    try:
        return value.strftime('%Y%m%d%H%M%S %z') if value else ''
    except AttributeError:
        return ''


class TvArea(scrapy.Item):
    item_name = 'area'
    get_item_loader = make_item_loader_method(TvAreaItemLoader)

    code = scrapy.Field(name='id', attrib=True)
    label = scrapy.Field(attrib='text')


class TvCategory(scrapy.Item):
    item_name = 'category'
    get_item_loader = make_item_loader_method(TvCategoryItemLoader)

    code = scrapy.Field(name='id', attrib=True)
    parent = scrapy.Field(attrib=True)
    label = scrapy.Field(attrib='text')


class TvChannel(scrapy.Item):
    item_name = 'channel'
    get_item_loader = make_item_loader_method(TvChannelItemLoader)

    code = scrapy.Field(name='id', attrib=True)
    area = scrapy.Field(attrib=True)
    title = scrapy.Field(name='display-name')


class TvProgrammeChannel(TvChannel):
    pass


class TvProgramme(scrapy.Item):
    item_name = 'programme'
    get_summary_item_loader = make_item_loader_method(TvProgrammeSummaryItemLoader)
    get_detail_item_loader = make_item_loader_method(TvProgrammeDetailItemLoader)

    code = scrapy.Field(name='id', attrib=True)
    tv_channel_code = scrapy.Field(name='channel', attrib=True)
    date_start = scrapy.Field(name='start', attrib=True, serializer=xmltv_datetime)
    date_stop = scrapy.Field(name='stop', attrib=True, serializer=xmltv_datetime)
    title = scrapy.Field()
    category = scrapy.Field(inline=True)
    desc = scrapy.Field(attrib='cdata')    # summary or detail
    summary = scrapy.Field(attrib='cdata')
    detail = scrapy.Field(attrib='cdata')
    credits = scrapy.Field()
    video = scrapy.Field()
    audio = scrapy.Field()
    iepg_link = scrapy.Field()
