# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from .items import (
    TvArea,
    TvCategory,
    TvChannel,
    TvProgramme,
)


class TvSonetBotPipeline(object):
    def __init__(self):
        self.area = {}
        self.channel = {}
        self.category = {}

    def process_item(self, item, spider):
        if isinstance(item, TvArea):
            self.area[item['code']] = item
            return item if 'area' in spider.filter_set else None

        if isinstance(item, TvCategory):
            self.category[item['code']] = item
            return item if 'category' in spider.filter_set else None

        if isinstance(item, TvChannel):
            self.channel[item['code']] = item
            return item if 'channel' in spider.filter_set else None

        if isinstance(item, TvProgramme):
            item['category'] = [self.category.get(el, el) for el in item['category']]

            if spider.desc:
                summary = item.pop('summary', None)
                detail = item.pop('detail', None)

                if spider.desc == 'summary':
                    item['desc'] = summary or detail

                if spider.desc == 'detail':
                    item['desc'] = detail or summary

        return item
