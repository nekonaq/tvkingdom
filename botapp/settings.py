# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

BOT_NAME = 'tvkingdom'

SPIDER_MODULES = ['botapp.spiders']
NEWSPIDER_MODULE = 'botapp.spiders'

TIME_ZONE = 'Japan'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 2
# RANDOMIZE_DOWNLOAD_DELAY = True

# CONCURRENT_REQUESTS = 16

# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN = 1
CONCURRENT_REQUESTS_PER_IP = 1

# DOWNLOAD_MAXSIZE = 1024*1024*1024          # 1024m
# DOWNLOAD_WARNSIZE = 32*1024*1024           # 32m

# CONCURRENT_ITEMS = 100

#-----------------------------------------------------------------------------
#** LogFormatter
# LOG_ENABLED = True
# LOG_ENCODING = 'utf-8'

LOG_FORMATTER = 'scrapy_extra.logformatter.LogFormatter'
# LOG_FORMAT = '%(asctime)s [%(name)s] %(levelname)s: %(message)s'
# LOG_DATEFORMAT = '%Y-%m-%d %H:%M:%S'
# LOG_STDOUT = False
# LOG_LEVEL = 'DEBUG'
# LOG_FILE = None

# LOG_CRAWLED_ENABLED = 1
# LOG_CRAWLED_FORMAT = "Crawled (%(status)s) %(request)s (referer: %(referer)s)%(flags)s"

LOG_SCRAPED_ENABLED = 0
# LOG_SCRAPED_FORMAT = "Scraped from %(src)s\n%(item)s"

# LOG_DROPPED_ENABLED = 1
# LOG_DROPPED_FORMAT = "Dropped: %(exception)s\n%(item)s"

#** Spider Middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
SPIDER_MIDDLEWARES = {
    #// Engine side
    # 'scrapy.spidermiddlewares.httperror.HttpErrorMiddleware': 50,
    # 'scrapy.spidermiddlewares.offsite.OffsiteMiddleware': 500,
    # 'scrapy.spidermiddlewares.referer.RefererMiddleware': 700,
    # 'scrapy.spidermiddlewares.urllength.UrlLengthMiddleware': 800,
    # 'scrapy.spidermiddlewares.depth.DepthMiddleware': 900,
    #// Spider side
}

#** HttpErrorMiddleware
# HTTPERROR_ALLOWED_CODES = []
# HTTPERROR_ALLOW_ALL = False

#** RefererMiddleware
# REFERER_ENABLED = True

#** UrlLengthMiddleware
# URLLENGTH_LIMIT = 2083

#** DepthMiddleware
# DEPTH_LIMIT = 0
# DEPTH_STATS = True
# DEPTH_PRIORITY = 0

#-----------------------------------------------------------------------------
#** Downloader Middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    #// Engine side
    # 'scrapy.downloadermiddlewares.robotstxt.RobotsTxtMiddleware': 100,
    # 'scrapy.downloadermiddlewares.httpauth.HttpAuthMiddleware': 300,
    # 'scrapy.downloadermiddlewares.downloadtimeout.DownloadTimeoutMiddleware': 350,
    # 'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': 400,
    # 'scrapy.downloadermiddlewares.retry.RetryMiddleware': 500,
    # 'scrapy.downloadermiddlewares.defaultheaders.DefaultHeadersMiddleware': 550,
    # 'scrapy.downloadermiddlewares.ajaxcrawl.AjaxCrawlMiddleware': 560,
    # 'scrapy.downloadermiddlewares.redirect.MetaRefreshMiddleware': 580,
    # 'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 590,
    # 'scrapy.downloadermiddlewares.redirect.RedirectMiddleware': 600,
    # 'scrapy.downloadermiddlewares.cookies.CookiesMiddleware': 700,
    # 'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 750,
    # 'scrapy.downloadermiddlewares.chunked.ChunkedTransferMiddleware': 830,
    # 'scrapy.downloadermiddlewares.stats.DownloaderStats': 850,
    # 'scrapy.downloadermiddlewares.httpcache.HttpCacheMiddleware': 900,
    #// Downloader side
}

#** RobotsTxtMiddleware
# Obey robots.txt rules
ROBOTSTXT_OBEY = True

#** RetryMiddleware
# Enable retries (enabled by default)
# RETRY_ENABLED = True
# RETRY_TIMES = 2
# RETRY_HTTP_CODES = [500, 502, 503, 504, 408]

#** DownloadTimeoutMiddleware
# DOWNLOAD_TIMEOUT = 180                     # 3mins

#** AjaxCrawlMiddleware
# AJAXCRAWL_ENABLED = False

#** MetaRefreshMiddleware
# METAREFRESH_ENABLED = True
# METAREFRESH_MAXDELAY = 100

#** HttpCompressionMiddleware
# COMPRESSION_ENABLED = True

#** DefaultHeadersMiddleware
DEFAULT_REQUEST_HEADERS = {
    'Accept': '*/*',
    'Accept-Language': 'ja,en-US;q=0.8,en;q=0.6',
}

#** RedirectMiddleware
# REDIRECT_ENABLED = True
# REDIRECT_MAX_TIMES = 20                    # uses Firefox default setting
# REDIRECT_PRIORITY_ADJUST = +2

#** CookiesMiddleware
# Enable cookies (enabled by default)
# COOKIES_ENABLED = True
# COOKIES_DEBUG = False

#** DownloaderStats
# DOWNLOADER_STATS = True

#** HttpCacheMiddleware
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
# Enable and configure HTTP caching (disabled by default)
# HTTPCACHE_ENABLED = False
# HTTPCACHE_DIR = 'httpcache'
# HTTPCACHE_IGNORE_MISSING = False
# HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
# HTTPCACHE_EXPIRATION_SECS = 0
# HTTPCACHE_ALWAYS_STORE = False
# HTTPCACHE_IGNORE_HTTP_CODES = []
# HTTPCACHE_IGNORE_SCHEMES = ['file']
# HTTPCACHE_IGNORE_RESPONSE_CACHE_CONTROLS = []
# HTTPCACHE_DBM_MODULE = 'anydbm' if six.PY2 else 'dbm'
# HTTPCACHE_POLICY = 'scrapy.extensions.httpcache.DummyPolicy'
# HTTPCACHE_GZIP = False

#-----------------------------------------------------------------------------
#** Extensions
# See http://scrapy.readthedocs.org/en/latest/topics/extensions.html
EXTENSIONS = {
    # 'scrapy.extensions.corestats.CoreStats': 0,
    # 'scrapy.extensions.telnet.TelnetConsole': 0,
    # 'scrapy.extensions.memusage.MemoryUsage': 0,
    # 'scrapy.extensions.memdebug.MemoryDebugger': 0,
    # 'scrapy.extensions.closespider.CloseSpider': 0,
    # 'scrapy.extensions.feedexport.FeedExporter': 0,
    # 'scrapy.extensions.logstats.LogStats': 0,
    # 'scrapy.extensions.spiderstate.SpiderState': 0,
    # 'scrapy.extensions.throttle.AutoThrottle': 0,
    'scrapy.extensions.feedexport.FeedExporter': None,
    'scrapy_extra.extensions.feedexport.FeedExporter': 0,
}

#** TelnetConsole
# Enable Telnet Console (enabled by default)
TELNETCONSOLE_ENABLED = False
# TELNETCONSOLE_PORT = [6023, 6073]
# TELNETCONSOLE_HOST = '127.0.0.1'

#** MemoryUsage
# MEMUSAGE_CHECK_INTERVAL_SECONDS = 60.0
# MEMUSAGE_ENABLED = False
# MEMUSAGE_LIMIT_MB = 0
# MEMUSAGE_NOTIFY_MAIL = []
# MEMUSAGE_REPORT = False
# MEMUSAGE_WARNING_MB = 0

#** MemoryDebugger
# MEMDEBUG_ENABLED = False           # enable memory debugging
# MEMDEBUG_NOTIFY = []               # send memory debugging report by mail at engine shutdown

#** CloseSpider
# CLOSESPIDER_TIMEOUT = 0
# CLOSESPIDER_PAGECOUNT = 0
# CLOSESPIDER_ITEMCOUNT = 0
# CLOSESPIDER_ERRORCOUNT = 0

#** FeedExporter
FEED_FORMAT = 'xml'

# FEED_STORE_EMPTY = False
# FEED_STORE_ITEMS = []
# FEED_TEMPDIR = None
# FEED_URI = None
# FEED_URI_PARAMS = None
FEED_ITEM_URI_PARAMS = 'botapp.feedexport.split_programme'

FEED_STORAGES = {
}

FEED_EXPORTERS = {
    'xml': 'scrapy_extra.exporters.xml.XmlItemExporter',
    'json': 'scrapy_extra.exporters.json.JsonLinesItemExporter',
    'yaml': 'scrapy_extra.exporters.yaml.YamlLinesItemExporter',
    'pp': 'scrapy_extra.exporters.pp.PprintItemExporter',

    'json.default': 'scrapy.exporters.JsonItemExporter',
    'jsonlines.default': 'scrapy.exporters.JsonLinesItemExporter',
    'jl.default': 'scrapy.exporters.JsonLinesItemExporter',
    'xml.default': 'scrapy.exporters.XmlItemExporter',
}

FEED_XMLITEMEXPORTER_PARAMS = {
    'root_element': 'tv',
}

FEED_JSONLINESITEMEXPORTER_PARAMS = {
    'ensure_ascii': False,
}

#** LogStats
# LOGSTATS_INTERVAL = 60.0

#** AutoThrottle
# AUTOTHROTTLE_ENABLED = False
# AUTOTHROTTLE_DEBUG = False
# AUTOTHROTTLE_START_DELAY = 5.0
# AUTOTHROTTLE_MAX_DELAY = 60.0
# AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0

#-----------------------------------------------------------------------------
#** Item Pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'botapp.pipelines.TvSonetBotPipeline': 300,
}
