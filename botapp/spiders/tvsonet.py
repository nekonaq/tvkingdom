# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
import time
import datetime
import urlparse
import urllib
import re
import scrapy
from scrapy.http import Request
from scrapy.loader.processors import TakeFirst
from scrapy_extra.utils.timezone import get_timezone
from ..items import (
    TvArea,
    TvCategory,
    TvChannel,
    TvProgrammeChannel,
    TvProgramme,
)


class TvSonetSpider(scrapy.Spider):
    name = "tvkingdom"

    SEARCH_URL = 'https://www.tvkingdom.jp/search/'            # 詳細検索; ジャンルを取得
    CHART_URL = 'https://www.tvkingdom.jp/chart/{area}.action'  # 番組表

    allowed_domains = ['www.tvkingdom.jp']

    custom_settings = {
        'TVSONET_SOURCE_TIME_ZONE': 'Japan',
    }

    def __init__(self, *args, **kwargs):
        kwargs = self.parse_initargs(**kwargs)
        super(TvSonetSpider, self).__init__(*args, **kwargs)

    def parse_initargs(self, **kwargs):
        self.area = kwargs.pop('area', '23')  # 地域のデフォルトは東京

        date_from_str = kwargs.pop('date', None)
        self.date_from = (
            datetime.date(*time.strptime(date_from_str, '%Y-%m-%d')[:3])
            if date_from_str
            else datetime.date.today())

        self.days = int(kwargs.pop('days', 1))

        # 以下の thead と tspan は主にデバッグ用
        self.thead = datetime.time(int(kwargs.pop('thead', 0)))  # 取得開始時
        self.tspan = int(kwargs.pop('tspan', 24))                # 取得時間

        # mode = [programme,summary,area,channel,category}
        #
        # mode = programme の場合:
        #   select_set=set(['category', 'channel', 'summary', 'detail'])
        #   filter_set=set(['programme', 'channel'])
        #   -> channel.xml と programme/{channel_id}.xml を出力。 detail あり
        #
        # mode = summary の場合:
        #    select_set=set(['category', 'channel', 'summary'])
        #    filter_set=set(['programme', 'channel', 'summary'])
        #    -> channel.xml と programme/{channel_id}.xml を出力。 detail なし
        #
        mode = kwargs.pop('mode', None)
        self.filter_set = set(mode.split(',')) if mode else {'programme'}
        self.select_set = self.filter_set & {'area', 'category', 'channel'}

        if 'programme' in self.filter_set:
            self.select_set.update(['category', 'channel', 'summary', 'detail'])
            self.filter_set.update(['channel', 'programme'])

        if 'summary' in self.filter_set:
            self.select_set.update(['category', 'channel', 'summary'])
            self.filter_set.update(['channel', 'programme'])

        exclude = kwargs.pop('exclude', None)
        exclude_set = set(exclude.split(',')) if exclude else set()
        self.select_set -= exclude_set

        if 'summary' not in self.select_set:
            # summary がなければ detail もなし
            self.select_set -= {'detail'}

        # desc = (none|summary|detail)
        self.desc = (lambda desc: None if desc == 'none' else desc)(kwargs.pop('desc', None))

        return kwargs

    LOGGING_OPTION_ITEMS = (
        'area',
        'date_from',
        'days',
        'thead',
        'tspan',
        'tz',
        'select_set',
        'filter_set',
        'desc',
    )

    def start_requests(self):
        self.tz = get_timezone(settings=self.settings, name='TVSONET_SOURCE_TIME_ZONE')
        datetime_from = datetime.datetime(
            *self.date_from.timetuple()[:3],
            hour=self.thead.hour,
            tzinfo=self.tz)

        self.logger.info(
            "Spider options: %s",
            ' '.join(['{}={}'.format(name, getattr(self, name))
                      for name in (self.LOGGING_OPTION_ITEMS)]))

        if self.select_set & {'area', 'category'}:
            yield self.make_request_for_search()

        if self.select_set & {'channel', 'summary', 'detail'}:
            for days in xrange(self.days):
                yield self.make_request_for_chart(datetime_from, days=days)

    def make_request_for_search(self):
        return Request(self.SEARCH_URL, dont_filter=True, callback=self.parse_search)

    def make_request_for_chart(self, datetime_from, days=0):
        datetime_at = datetime_from + datetime.timedelta(days=days)

        els = urlparse.urlsplit(self.CHART_URL.format(area=self.area))._asdict()
        els['query'] = urllib.urlencode({
            'iepgType': 1,
            'head': datetime_at.strftime('%Y%m%d%H%M'),
            'span': self.tspan,
        })

        url = urlparse.SplitResult(**els).geturl()
        return Request(url, dont_filter=True, meta={
            'datetime_from': datetime_from,
            'datetime_at': datetime_at,
        })

    def parse(self, response):
        if 'channel' in self.select_set:
            # 放送局の取得は最初のページだけ行う
            if response.meta['datetime_at'] == response.meta['datetime_from']:
                for item in self.scrape_channel_from_chart(response):
                    yield item

        if 'summary' in self.select_set:
            for item in self.scrape_programme_from_chart(response):
                yield item

    def scrape_programme_from_chart(self, response):
        """番組を取得する
        """
        for sel in response.xpath('//div[starts-with(@id, "cell-") and contains(@class, "cell-schedule")]'):
            it = TvProgramme.get_summary_item_loader(response=response, selector=sel, tz=self.tz)

            # @id は cell_id: 'cell-{番組コード}-{数字} の形式。 e.g. 'cell-101032201607230125-20'
            it.add_xpath('code', '@id', re=r'cell-(\d+)-\d+')
            it.add_xpath('title', './/span[@class="schedule-title" or @class="schedule-titleC"]/node()')
            it.add_xpath('summary', './/span[@class="schedule-summary" or @class="schedule-summaryC"]/node()')
            it.add_xpath('category', '@class', re=r'cell-genre-(\d+)')
            it.add_xpath('iepg_link', './/a[@name="iepg-button"]/@href')

            item = it.load_item()
            if response.meta['datetime_from'] <= item['date_start'] < response.meta['datetime_at']:
                # 前日からの継続で重複しているのでスキップ
                continue

            if 'detail' not in self.select_set:
                yield item
            else:
                link = it.get_xpath('.//a[@class="schedule-link"]/@href', TakeFirst())
                if link:
                    detail_link = urlparse.urljoin(response.url, link)
                    yield Request(detail_link, callback=self.parse_detail, meta={'programme': item})

    def scrape_channel_from_chart(self, response):
        """放送局を取得する
        """
        for sel in response.xpath('//div[starts-with(@id, "cell-station-top-")]'):
            it = TvChannel.get_item_loader(selector=sel)

            it.add_xpath('code', '@id', re=r'cell-station-top-(\d+)')  # 放送局コード、 6桁の数字。
            it.add_xpath('title', '@title')

            item = it.load_item()
            yield item
            if 'programme' in self.filter_set:
                yield TvProgrammeChannel(item)

    def parse_search(self, response):
        if 'area' in self.select_set:
            for item in self.scrape_area_from_search(response):
                yield item

        if 'category' in self.select_set:
            for item in self.scrape_category_from_search(response):
                yield item

    def scrape_area_from_search(self, response):
        for form in response.xpath('//form[@name="changeStationAreaForm"]'):
            for sel in form.xpath('.//select[@id="area-selector"]/option'):
                it = TvArea.get_item_loader(selector=sel)

                it.add_xpath('code', '@value')
                it.add_xpath('label', 'text()')

                item = it.load_item()
                yield item

    def scrape_category_from_search(self, response):
        for form in response.xpath('//form[@name="mainForm"]'):
            for sel in form.xpath('.//select[@id="condition_genres_0__parentId"]/option'):
                it = TvCategory.get_item_loader(selector=sel)

                it.add_xpath('code', '@value')
                it.add_xpath('label', 'text()')
                item = it.load_item()
                if 'code' not in item:
                    continue

                yield item

            js = form.xpath('.//script').extract_first()  # 最初の javascript
            for mc in re.finditer(r'new Option\("(?P<label>[^"]+)", "(?P<code>\d+)"\)', js):
                it = TvCategory.get_item_loader()

                code = mc.group('code')
                it.add_value('code', code)
                it.add_value('label', mc.group('label'))

                item = it.load_item()
                yield item


    RE_AIRTIME = re.compile(
        r'(?P<month>\d+)/(?P<day>\d+).+?'
        '(?P<sh>\d+):(?P<sm>\d+)[^\d]+(?P<eh>\d+):(?P<em>\d+)')

    def parse_detail(self, response):
        it = TvProgramme.get_detail_item_loader(
            item=response.meta['programme'],
            response=response)

        # 番組情報
        itcb = it.nested_xpath('//div[@class="contBlock utileSetting"]')

        # //番組タイトル
        # title = itcb.get_xpath('//dd[1]/text()', TakeFirst(), unicode.strip)

        # //放送時間; e.g. "9/17 (土) 7:00 ～ 7:30 （30分）"
        # airtime = itcb.get_xpath('//dd[2]/text()', TakeFirst(), unicode.strip)
        airtime = itcb.get_xpath('//dl[@class="basicTxt"]/dd[1]/text()', TakeFirst(), unicode.strip)  # 2021-02-01
        mc = self.RE_AIRTIME.match(airtime)

        date_start = itcb.item['date_start']
        date_stop = date_start.replace(
            hour=int(mc.group('eh')),
            minute=int(mc.group('em')))

        if date_start > date_stop:
            date_stop += datetime.timedelta(days=1)

        itcb.add_value('date_stop', date_stop)

        # //放送局名; e.g. "ＮＨＫ総合・東京(Ch.1)"
        # channel_name = itcb.get_xpath('//dd[3]/text()', TakeFirst())

        # //カテゴリ
        # categories = itcb.get_xpath('//dd[4]//a')

        # 番組概要・番組詳細・人名リンク

        # //番組概要; disabled
        # itcs1 = it.nested_xpath('//div[@class="contBlock subUtileSetting"][1]')
        # summary = itcs1.get_xpath('p[@class="basicTxt"]/node()')

        # //番組詳細、映像情報
        itcs2 = it.nested_xpath('//div[@class="contBlock subUtileSetting"][2]')
        itcs2.add_xpath('detail', 'p[@class="basicTxt"]/node()')
        itcs2.add_xpath('video', 'div[@class="basicTxt"]/span/text()')

        # //人名リンク
        itcs3 = it.nested_xpath('//div[@class="contBlock subUtileSetting"][3]')
        itcs3.add_xpath('credits', 'p[@class="basicTxt"]/a/text()')

        item = it.load_item()
        yield item
