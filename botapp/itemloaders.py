# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
import six
import datetime
import urlparse
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Compose, MapCompose, Identity, TakeFirst, Join


def proc_remove_empty(value):
    return value or None


def proc_skip_wbr(value):
    return None if value == '<wbr></wbr>' else value


def proc_replace_br(value):
    return '\n' if value == '<br>' else value


def proc_replace_nl2(value):
    return value.replace('\n\n', '<br />')


def proc_replace_br2(value):
    return value.replace('<br><br>', '\n').rstrip().replace('\n', '<br />')


def proc_parse_datetime(value, loader_context):
    if not isinstance(value, six.string_types):
        return value

    tz = loader_context.get('tz')
    return tz.localize(datetime.datetime.strptime(value, '%Y%m%d%H%M'))


UNICODE_LDQUO = u'\u201c'                  # &ldquo; LEFT DOUBLE QUOTATION MARK
UNICODE_RDQUO = u'\u201d'                  # &rdquo; RIGHT_DOUBLE_QUOTATION_MARK


def proc_fix_rdquo(value):
    return value.replace('"', UNICODE_RDQUO)


def proc_ignore_negative(value):
    return None if value.startswith('-') else value


def proc_set_parent_category_code(value, loader_context):
    item = loader_context.get('item')
    if not value.endswith('000'):
        item['parent'] = '{}000'.format(value[:3])

    return value


def proc_programme_code(value, loader_context):
    """
    value は番組コード: 'ssssssYYmmddHHMM' の形式。
    ssssss は 6桁の放送局id。
    YYmmddHHMM は放送開始時刻

    e.g.
    番組コード:	'101032201607230125'
    放送局id:	'101032'
    開始時刻:	2016-07-23 01:25'
    """
    item = loader_context.get('item')
    item['tv_channel_code'] = value[:6]
    item['date_start'] = proc_parse_datetime(value[6:], loader_context)
    return value


def proc_iepg_link(value, loader_context):
    response = loader_context.get('response')
    iepg_link = urlparse.urljoin(response.url, value)
    if not loader_context.get('store_iepg_link', False):
        # この値は itm に格納しない
        return None
    return iepg_link


class TvAreaItemLoader(ItemLoader):
    default_output_processor = TakeFirst()


class TvCategoryItemLoader(ItemLoader):
    default_output_processor = TakeFirst()
    code_in = MapCompose(proc_ignore_negative, proc_set_parent_category_code)


class TvChannelItemLoader(ItemLoader):
    default_output_processor = TakeFirst()


class TvProgrammeSummaryItemLoader(ItemLoader):
    default_output_processor = TakeFirst()

    code_in = MapCompose(proc_programme_code)

    title_out = Compose(MapCompose(proc_skip_wbr), Join(separator=''))
    summary_out = Compose(
        MapCompose(proc_skip_wbr, unicode.lstrip),
        Join(separator=''),
        proc_replace_nl2,
        proc_remove_empty,
    )
    category_out = Identity()
    iepg_link_out = Compose(MapCompose(proc_iepg_link), TakeFirst())


class TvProgrammeDetailItemLoader(ItemLoader):
    default_output_processor = TakeFirst()

    detail_out = Compose(
        MapCompose(unicode.lstrip),
        Join(separator=''),
        proc_replace_br2,
        proc_remove_empty,
    )
    credits_out = Identity()
    video_out = Identity()
