FROM buildpack-deps:18.04

LABEL maintainer="Tatsuo Nakajyo <tnak@nekonaq.com>"

ENV LANG C.UTF-8
ENV TZ Japan
ENV PYTHONUNBUFFERED 1
ENV PYTHONWARNINGS ignore:DEPRECATION

WORKDIR /app

COPY docker-entrypoint /
COPY requirements*.txt scrapy.cfg /app/
COPY bin /app/bin/
COPY config /app/config/
COPY botapp /app/botapp/
COPY scrapy-extra /app/scrapy-extra/
COPY dump-etree /app/dump-etree/

# - cryptography==2.2.2 は "CryptographyDeprecationWarning: Support for your Python version
#   is deprecated..." を抑制するために必要。
#
# - そして cryptography==2.2.2 は pyopenssl~=19.1.0 と互換性がなく、このことに由来する
#   warning を抑制するため pyopenssl~=18.0.0 を使う。
#
# - ignore:DEPRECATION は "DEPRECATION: Python 2.7 reached the end of its life..." を抑制す
#   るために必要。
#
RUN set -x \
&& export DEBIAN_FRONTEND=noninteractive \
&& export PYTHONWARNINGS=ignore:DEPRECATION \
&& apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y python3 python3-pip python3-apt libyaml-dev \
&& apt-get install -y python2.7 python-pip python-pip-whl libxslt1-dev libxml2-dev libssl-dev libffi-dev libyaml-dev rsync \
&& . bin/setup-sitecustomize.sh \
&& python -mpip install --upgrade pip setuptools cryptography==2.2.2 pyopenssl~=18.0.0 ansible~=2.8.0 \
&& bin/install-prereq \
&& python -mpip install -r requirements.txt \
&& mkdir -p /app/data \
&& ( set +x; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ) \
;

ENTRYPOINT ["/docker-entrypoint"]
