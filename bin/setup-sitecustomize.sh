# Usage: source setup-sitecustomize.sh
( set -e
  sitecustomize_path="$( python -c 'import site; print site.getsitepackages()[0]' )/sitecustomize.py"
  if [ -e $sitecustomize_path ]; then
      echo "file already exists: $sitecustomize_path" >&2
      exit
  fi
  echo "creating: $sitecustomize_path" >&2
  cat <<EOF >$sitecustomize_path
import sys
sys.setdefaultencoding('utf-8')
EOF
)
